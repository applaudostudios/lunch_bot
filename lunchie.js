const Botkit = require('botkit');
const fs = require('fs');
const dotEnv = require('dotenv').config();

const controller = Botkit.slackbot({
  stats_optout: true,
});

const theBot = controller.spawn({
  token: process.env.token,
}).startRTM();


controller.hears('list', 'direct_message,direct_mention,mention', (bot, message) => {
  const reply = {
    username: 'lunchie',
    text: "Here's the list of food places: Villaflores, LIO, Florencia.",
    icon_url: process.env.icon_url,
  };

  bot.reply(message, reply);
});

controller.hears('menu (.*)', 'direct_message,direct_mention,mention', (bot, message) => {
  const reply = {
    username: 'lunchie',
    text: '',
    icon_url: process.env.icon_url,
    attachments: [],
  };

  const attachment = {
    title: '',
    title_link: '',
    pretext: '',
  };

  const menuName = message.match[1];

  const menuFileName = `${menuName.toLowerCase()}.txt`;

  if (fs.existsSync(menuFileName)) {
    const contents = fs.readFileSync(menuFileName, 'utf8');

    attachment.title = `Menu from ${menuName}`;
    attachment.text = `\`\`\`${contents}\`\`\``;
    attachment.color = '#7CD197';

    reply.text = `Here's the menu you requested from ${menuName}`;
    reply.attachments.push(attachment);
  } else {
    reply.text = 'Oops! Bryan has not posted that menu yet.';
  }
  bot.reply(message, reply);
});

controller.hears('order', 'direct_message,direct_mention,mention', (bot, message) => {
  const reply = {
    username: 'lunchie',
    text: "Great! Here's the form to request your meal.",
    icon_url: process.env.icon_url,
    attachments: [],
  };

  const attachment = {
    title: 'Menu Form',
    title_link: 'https://docs.google.com/a/applaudostudios.com/forms/d/e/1FAIpQLScpf8Ps2dVFV0BIcgtb7vRIRzdECSEJ0PRm95sMaZAn_TMBig/viewform?c=0&w=1',
    text: 'Todo lo del anterior pero mejorado :)',
    color: '#7CD197',
  };

  reply.attachments.push(attachment);
  bot.reply(message, reply);
});

controller.hears('help', 'direct_message,direct_mention,mention', (bot, message) => {
  const reply = {
    username: 'lunchie',
    text: 'Here\'s the list of things I can do for you.\n' +
          '1. *List*: This will show you the list of places.\n' +
          '2. *Menu <name>*: It will look for the menu of that place. e.g. menu villaflores.\n' +
          '3. *Order*: This will simply post the link to our awesome form.',
    icon_url: process.env.icon_url,
  };

  bot.reply(message, reply);
});

controller.on('rtm_close', (bot) => {
  bot.startRTM((err) => {
    if (!err) {
      process.exit();
    }
  });
});
