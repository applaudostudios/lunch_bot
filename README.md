### Lunch Bot Readme ###

* A simple bot that let's you know the menu of all the food places we usually order from
* 0.1


### How do I get set up? ###

1. Clone repository
2. Install dependencies: npm install
3. Setup your .env file (sample file provided)
4. Start the lunchie.js file
5. Enjoy!